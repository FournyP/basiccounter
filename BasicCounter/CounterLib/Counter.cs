﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterLib
{
    public class Counter
    {
        private static int valeur = 0;

        public int Get_Compteur()
        {
            return Counter.valeur;
        }

        public void Set_Compteur(int value)
        {
            Counter.valeur = value;
        }

        public void Add_Compteur()
        {
            Counter.valeur = Get_Compteur() + 1;
        }

        public void Remove_Compteur()
        {
            Counter.valeur = Get_Compteur() - 1;
        }

        public void RAZ_Compteur()
        {
            Counter.valeur = 0;
        }
    }
}
