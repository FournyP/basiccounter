﻿Imports CounterLib

Public Class frm_BasicCounter
    Private Sub cmd_remove_Click(sender As Object, e As EventArgs) Handles cmd_remove.Click
        Dim compteur As New Counter
        compteur.Remove_Compteur()
        lbl_counter.Text = compteur.Get_Compteur()
    End Sub

    Private Sub cmd_add_Click(sender As Object, e As EventArgs) Handles cmd_add.Click
        Dim compteur As New Counter
        compteur.Add_Compteur()
        lbl_counter.Text = compteur.Get_Compteur()
    End Sub

    Private Sub cmd_RAZ_Click(sender As Object, e As EventArgs) Handles cmd_RAZ.Click
        Dim compteur As New Counter
        compteur.RAZ_Compteur()
        lbl_counter.Text = compteur.Get_Compteur()
    End Sub
End Class
