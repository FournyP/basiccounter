﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmd_remove = New System.Windows.Forms.Button()
        Me.cmd_add = New System.Windows.Forms.Button()
        Me.lbl_Total = New System.Windows.Forms.Label()
        Me.lbl_counter = New System.Windows.Forms.Label()
        Me.cmd_RAZ = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmd_remove
        '
        Me.cmd_remove.Location = New System.Drawing.Point(110, 211)
        Me.cmd_remove.Name = "cmd_remove"
        Me.cmd_remove.Size = New System.Drawing.Size(150, 35)
        Me.cmd_remove.TabIndex = 0
        Me.cmd_remove.Text = "-"
        Me.cmd_remove.UseVisualStyleBackColor = True
        '
        'cmd_add
        '
        Me.cmd_add.Location = New System.Drawing.Point(535, 211)
        Me.cmd_add.Name = "cmd_add"
        Me.cmd_add.Size = New System.Drawing.Size(150, 35)
        Me.cmd_add.TabIndex = 1
        Me.cmd_add.Text = "+"
        Me.cmd_add.UseVisualStyleBackColor = True
        '
        'lbl_Total
        '
        Me.lbl_Total.AutoSize = True
        Me.lbl_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Total.Location = New System.Drawing.Point(350, 134)
        Me.lbl_Total.Name = "lbl_Total"
        Me.lbl_Total.Size = New System.Drawing.Size(101, 42)
        Me.lbl_Total.TabIndex = 2
        Me.lbl_Total.Text = "Total"
        Me.lbl_Total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_counter
        '
        Me.lbl_counter.AutoSize = True
        Me.lbl_counter.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_counter.Location = New System.Drawing.Point(382, 204)
        Me.lbl_counter.Name = "lbl_counter"
        Me.lbl_counter.Size = New System.Drawing.Size(39, 42)
        Me.lbl_counter.TabIndex = 3
        Me.lbl_counter.Text = "0"
        '
        'cmd_RAZ
        '
        Me.cmd_RAZ.AutoSize = True
        Me.cmd_RAZ.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_RAZ.Location = New System.Drawing.Point(348, 295)
        Me.cmd_RAZ.Name = "cmd_RAZ"
        Me.cmd_RAZ.Size = New System.Drawing.Size(103, 52)
        Me.cmd_RAZ.TabIndex = 4
        Me.cmd_RAZ.Text = "RAZ"
        Me.cmd_RAZ.UseVisualStyleBackColor = True
        '
        'frm_BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.cmd_RAZ)
        Me.Controls.Add(Me.lbl_counter)
        Me.Controls.Add(Me.lbl_Total)
        Me.Controls.Add(Me.cmd_add)
        Me.Controls.Add(Me.cmd_remove)
        Me.Name = "frm_BasicCounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmd_remove As Button
    Friend WithEvents cmd_add As Button
    Friend WithEvents lbl_Total As Label
    Friend WithEvents lbl_counter As Label
    Friend WithEvents cmd_RAZ As Button
End Class
