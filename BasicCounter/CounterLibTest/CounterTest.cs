﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CounterLib;
namespace CounterLibTest
{
    [TestClass]
    public class CounterTest
    {
        [TestMethod]
        public void Get_CompteurTest()
        {
            Counter compteur = new Counter();
            compteur.Set_Compteur(0);
            Assert.AreEqual(0, compteur.Get_Compteur());
            compteur.Set_Compteur(2);
            Assert.AreEqual(2, compteur.Get_Compteur());
        }

        [TestMethod]
        public void Set_CompteurTest()
        {
            Counter compteur = new Counter();
            compteur.Set_Compteur(0);
            Assert.AreEqual(0, compteur.Get_Compteur());
        }

        [TestMethod]
        public void Add_CompteurTest()
        {
            Counter compteur = new Counter();
            compteur.Set_Compteur(0);
            compteur.Add_Compteur();
            Assert.AreEqual(1, compteur.Get_Compteur());
        }

        [TestMethod]
        public void Remove_CompteurTest()
        {
            Counter compteur = new Counter();
            compteur.Set_Compteur(0);
            compteur.Remove_Compteur();
            Assert.AreEqual(-1, compteur.Get_Compteur());
        }

        [TestMethod]
        public void RAZ_CompteurTest()
        {
            Counter compteur = new Counter();
            compteur.Set_Compteur(3);
            compteur.RAZ_Compteur();
            Assert.AreEqual(0, compteur.Get_Compteur());
        }
    }
}
